//
//  PlayerViewController.swift
//  Streamer-iOS-CP
//
//  Created by Michael Konapelsky on 7/29/17.
//
//

import UIKit
import AVKit
import AVFoundation

class PlayerViewController: UIViewController {

    var videoURL: String!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        let url: URL = URL(string: videoURL)!
        let player: AVPlayer = AVPlayer(url: url)
        
        let playerController: AVPlayerViewController = AVPlayerViewController()
        playerController.player = player
        
        self.addChildViewController(playerController)
        self.view.addSubview(playerController.view)
        playerController.view.frame = self.view.frame;
        
        player.play()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}
