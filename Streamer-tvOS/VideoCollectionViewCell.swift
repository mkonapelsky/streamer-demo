//
//  VideoCollectionViewCell.swift
//  Streamer-iOS-CP
//
//  Created by Michael Konapelsky on 7/30/17.
//
//

import UIKit

class VideoCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet var thumbNail: UIImageView!
    @IBOutlet var lblAuthor: UILabel!
    @IBOutlet var lblViews: UILabel!
}
