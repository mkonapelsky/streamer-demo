//
//  ViewController.swift
//  Streamer-tvOS
//
//  Created by Michael Konapelsky on 7/28/17.
//
//

import UIKit

class ViewController: UIViewController, UITextFieldDelegate, ResponseHitThumbnailDelegate, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    private var _searchObject: ResponseObject = ResponseObject()
    private let itemsPerRow: CGFloat = 3
    private let sectionInsets = UIEdgeInsets(top: 50.0, left: 20.0, bottom: 50.0, right: 20.0)
    @IBOutlet private var searchField: UITextField!
    @IBOutlet private var resultsCollection: UICollectionView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        self.title = "Video Search Demo"
        
        self.navigationController?.navigationBar.barTintColor = UIColor.black
        
        searchField.backgroundColor = UIColor.white
        searchField.placeholder = "Search"
        searchField.textAlignment = .center
        searchField.keyboardType = .webSearch
        searchField.delegate = self
        
        resultsCollection.delegate = self
        resultsCollection.dataSource = self
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func searchMovies(searchWords: String) {
        
        SVProgressHUD.show(withStatus: "Searching ...")
        
        let web = WebManager()
        web.startGETRequest(uri: "https://pixabay.com/api/videos/?key=6024963-a84e7b583b488d22a4cffe1c0&q=" + searchWords.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!) {
            responseData, responseError in
            
            if responseError != nil
            {
                self.present(Utilities.ShowBasicAlert(title: "Error", message: responseError!, style: .actionSheet), animated: true, completion: nil)
            }
            else
            {
                do {
                    
                    let jsonObject = try JSONSerialization.jsonObject(with: responseData!, options: JSONSerialization.ReadingOptions.allowFragments)
                    
                    self._searchObject = ResponseObject.buildWithJSON(json: jsonObject as! Dictionary<String, AnyObject>)
                    self._searchObject.thumbnailDelegate = self;
                    
                    if self._searchObject.hits.count == 0 {
                        
                        self.present(Utilities.ShowBasicAlert(title: "No Videos",
                                                              message: "Couldn't find videos based on your search results.",
                                                              style: .alert),
                                     animated: true)
                        
                        SVProgressHUD.dismiss()
                        
                    }
                    else {
                        
                        SVProgressHUD.show(withStatus: "Generating Thumbnails ...")
                        self._searchObject.GetVideoThumbnails();
                        
                    }
                }
                catch
                {
                    // Alert
                    self.present(Utilities.ShowBasicAlert(title: "Attention", message: "There was a problem refreshing. {1}", style: .actionSheet), animated: true, completion: nil)
                }
            }
        }
    }
    
    // Collection View Protocol
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return self._searchObject.hits.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell: VideoCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "videoCell", for: indexPath) as! VideoCollectionViewCell
        
        let object: ResponseHit = self._searchObject.hits[indexPath.row]
        
        cell.thumbNail.image = object.thumbnail
        cell.lblAuthor.text = object.user
        cell.lblViews.text = "\(object.views!) Views"
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let object: ResponseHit = self._searchObject.hits[indexPath.row]
        
        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let playerView: PlayerViewController = storyBoard.instantiateViewController(withIdentifier: "playerView") as! PlayerViewController
        
        playerView.videoURL = object.videoURL
        
        self.navigationController?.present(playerView, animated: true, completion: nil)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let paddingSpace = sectionInsets.left * (itemsPerRow + 1)
        let availableWidth = view.frame.width - paddingSpace
        let widthPerItem = availableWidth / itemsPerRow
        
        return CGSize(width: widthPerItem, height: widthPerItem)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        
        return sectionInsets
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        
        return sectionInsets.left
    }
    
    // Text Field Protocol
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        // Perform search, dismiss keyboard
        searchMovies(searchWords: textField.text!)
        textField.resignFirstResponder()
        
        return true
    }
    
    // Thumbnail delegate
    func didFinishBuildingThumbnails(responseObject: ResponseObject) {
        
        SVProgressHUD.dismiss()
        self.resultsCollection.reloadData()
    }
}

