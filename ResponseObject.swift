//
//  ResponseObject.swift
//  Streamer-iOS-CP
//
//  Created by Michael Konapelsky on 7/28/17.
//
//

import UIKit
import AVKit
import AVFoundation

protocol ResponseHitThumbnailDelegate {
    func didFinishBuildingThumbnails(responseObject: ResponseObject) -> Void
}

class ResponseObject: NSObject {

    var total: Int?
    var totalHits: Int?
    var hits: [ResponseHit] = [ResponseHit]()
    
    var thumbnailDelegate: ResponseHitThumbnailDelegate?
    
    override init() { }
    
    init(delegate: ResponseHitThumbnailDelegate?) {
        
        thumbnailDelegate = delegate
    }
    
    class func buildWithJSON(json: Dictionary<String, AnyObject>) -> ResponseObject {
        
        let respObject = ResponseObject()
        
        respObject.total = json["total"] as? Int
        respObject.totalHits = json["totalHits"] as? Int
        
        if let hits = json["hits"] as? [[String : AnyObject]] {
            
            respObject.hits = [ResponseHit]()
            for hit in hits as [[String : AnyObject]] {
                
                respObject.hits.append(ResponseHit.buildWithJSON(json: hit))
            }
        }
        
        return respObject
    }
    
    func GetVideoThumbnails() -> Void {
        
        DispatchQueue.global(qos: .userInitiated).async {
            
            for hit in self.hits {
                
                let asset: AVAsset = AVAsset(url: URL(string: hit.videoURL!)!)
                let midPoint = CMTimeMakeWithSeconds(asset.duration.seconds / 2, 600)
                
                let generator: AVAssetImageGenerator = AVAssetImageGenerator(asset: asset)
                generator.appliesPreferredTrackTransform = true
                
                do {
                    let ref: CGImage = try generator.copyCGImage(at: midPoint, actualTime: nil)
                    
                    /// Resize
                    let image = UIImage(cgImage: ref)
                    
                    let size = image.size.applying(CGAffineTransform(scaleX: 0.5, y: 0.5))
                    let hasAlpha = false
                    let scale: CGFloat = 0.0 // Automatically use scale factor of main screen
                    
                    UIGraphicsBeginImageContextWithOptions(size, !hasAlpha, scale)
                    image.draw(in: CGRect(origin: CGPoint.zero, size: size))
                    
                    let scaledImage = UIGraphicsGetImageFromCurrentImageContext()
                    UIGraphicsEndImageContext()
                    
                    hit.thumbnail = scaledImage
                }
                catch {
                    
                    // Good place for error logging or crash analytics
                    hit.thumbnail = nil
                }
            }
            
            DispatchQueue.main.async {
                self.thumbnailDelegate?.didFinishBuildingThumbnails(responseObject: self)
            }
        }
        
    }
}
