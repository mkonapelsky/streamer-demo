//
//  WebManager.swift
//  KampKeeper
//
//  Created by Mike Konapelsky on 6/16/16.
//  Copyright © 2016 Lehigh Interactive. All rights reserved.
//

import Foundation

protocol WebManagerDelegate {
    func webManagerDidStartConnection() -> Void
    func webManagerDidFailConnection(_ errorMessage: String) -> Void
    func webManagerDidFinishConnection(_ responseString: String) -> Void
    func webManagerDidFinishConnection(data: Data) -> Void
}

class WebManager : NSObject, URLSessionDelegate, URLSessionDataDelegate {
    
    var webManagerDelegate: WebManagerDelegate?
    var dataVal = NSMutableData()
    
    var urlSession: URLSession?
    var dataTask: URLSessionDataTask?
    
    override init() {
        
        urlSession = URLSession(configuration: URLSessionConfiguration.default)
    }
    
    init(delegate: WebManagerDelegate)
    {
        webManagerDelegate = delegate;
        urlSession = URLSession(configuration: URLSessionConfiguration.default)
    }
    
    /**
     Perform an HTTP POST request to the supplied URI. Response information is communicated via the completion block. This method does not post an HTTP Body
     
     @param uri The URI to perform the request against.
     @param completion The block used to communicate completion data.
     */
    func startPOSTRequest(withURI uri: String, completion: @escaping (_ responseData: Data?, _ responseError: String?) -> Void) {
        
        if dataTask != nil
        {
            dataTask?.cancel()
        }
        
        var request = URLRequest(url: URL(string: uri)!)
        request.httpMethod = "POST"
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("0", forHTTPHeaderField: "Content-Length")
        
        dataTask = urlSession?.dataTask(with: request as URLRequest) {
            (data, response, error) in
            
            DispatchQueue.main.async {
                
            }
            
            if let error = error
            {
                completion(nil, error.localizedDescription)
            }
            else if let httpResponse = response as? HTTPURLResponse
            {
                if httpResponse.statusCode == 200
                {
                    completion(data!, nil)
                }
                else
                {
                    completion(nil, "Failed: \(httpResponse.statusCode) - BODY: \(String(describing: String(data: data!, encoding: String.Encoding.utf8)))")
                }
            }
        }
        
        dataTask?.resume()
    }
    
    /**
     Perform an HTTP POST request to the supplied URI. Response information is communicated via the completion block.
     
     @param uri The URI to perform the request against.
     @param payload The JSON object to be sent server-side as the HTTP Request Body
     @param completion The block used to communicate completion data.
     */
    func startPOSTRequest(withURI uri: String, payload: [String : AnyObject]?, completion: @escaping (_ responseData: Data?, _ responseError: String?) -> Void) {
        
        do {
            
            var postBodyData: Data?
            
            if payload != nil {
                postBodyData = try JSONSerialization.data(withJSONObject: payload!, options: JSONSerialization.WritingOptions.prettyPrinted)
            }
            
            if dataTask != nil
            {
                dataTask?.cancel()
            }
            
            var request = URLRequest(url: URL(string: uri)!)
            request.httpMethod = "POST"
            request.addValue("application/json", forHTTPHeaderField: "Content-Type")
            request.addValue("\(postBodyData!.count)", forHTTPHeaderField: "Content-Size")
            
            request.httpBody = postBodyData
            
            dataTask = urlSession?.dataTask(with: request as URLRequest) {
                (data, response, error) in
                
                DispatchQueue.main.async {
                    
                }
                
                if let error = error
                {
                    completion(nil, error.localizedDescription)
                }
                else if let httpResponse = response as? HTTPURLResponse
                {
                    if httpResponse.statusCode == 200
                    {
                        completion(data!, nil)
                    }
                    else
                    {
                        completion(nil, "Failed: \(httpResponse.statusCode)")
                    }
                }
            }
            
            dataTask?.resume()
            
        }
        catch
        {
            completion(nil, "Plain old broken")
        }
    }
    
    /**
     Perform an HTTP GET request to the supplied URI. Response information is communicated via the completion block.
     
     @param uri The URI to perform the request against.
     @param completion The block used to communicate completion data.
     */
    func startGETRequest(uri: String, completion: @escaping (_ responseData: Data?, _ resposeError: String?) -> Void) {
        
        if dataTask != nil
        {
            dataTask?.cancel()
        }
        
        dataTask = urlSession?.dataTask(with: URL(string: uri)!) {
            data, response, error in
            
            DispatchQueue.main.async {
                
                if let error = error
                {
                    print(error.localizedDescription)
                    completion(nil, error.localizedDescription)
                }
                else if let httpResponse = response as? HTTPURLResponse
                {
                    if httpResponse.statusCode == 200
                    {
                        completion(data!, nil)
                    }
                    else
                    {
                        completion(nil, "Request failed: Status Code \(httpResponse.statusCode)")
                    }
                }
            }
        }
        
        dataTask?.resume()
    }
    
    /**
     Perform an HTTP GET request to the supplied URI. Response information is communicated via the WebManagerDelegate protocol.
     
     @param uri The URI to perform the request against.
     */
    func startGETRequest(uri: String) -> Void
    {
        if dataTask != nil
        {
            dataTask?.cancel()
        }
        
        dataTask = urlSession?.dataTask(with: URL(string: uri)!) {
            data, response, error in
            
            DispatchQueue.main.async {
                
                if let error = error
                {
                    print(error.localizedDescription)
                    self.webManagerDelegate!.webManagerDidFailConnection(error.localizedDescription)
                }
                else if let httpResponse = response as? HTTPURLResponse
                {
                    if httpResponse.statusCode == 200
                    {
                        self.webManagerDelegate!.webManagerDidFinishConnection(data: data!)
                    }
                    else
                    {
                        self.webManagerDelegate!.webManagerDidFinishConnection("Request failed: Status Code \(httpResponse.statusCode)")
                    }
                }
            }
        }
        
        dataTask?.resume()
    }
}
