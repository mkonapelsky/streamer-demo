//
//  Utilities.swift
//  Streamer-iOS-CP
//
//  Created by Michael Konapelsky on 7/28/17.
//
//

import UIKit

class Utilities: NSObject {

    
    /**
     Create a basic alert used to convey information.
     
     @param title The title that should be displayed on the alert
     @param message The message to display.
     @param style The style to use for the alert. (.actionSheet, .alert)
     
     @return A ready to use UIActionController.
     */
    class func ShowBasicAlert(title: String, message: String, style: UIAlertControllerStyle) -> UIAlertController {
        
        let alert = UIAlertController(title: title, message: message, preferredStyle: style) // .alert
        
        let defaultAction = UIAlertAction(title: "OK", style: .default, handler: nil)
        alert.addAction(defaultAction)
        
        return alert
    }
}
