//
//  ResponseHit.swift
//  Streamer-iOS-CP
//
//  Created by Michael Konapelsky on 7/28/17.
//
//

import UIKit

class ResponseHit: NSObject {
    
    var id: Int?
    var pageURL: String?
    var type: String?
    var tags: String?
    var duration: Int?
    var pictureId: String?
    var views: Int?
    var downloads: Int?
    var favorites: Int?
    var likes: Int?
    var comments: Int?
    var userId: Int?
    var user: String?
    var userImageURL: String?
    var videoURL: String?
    var videoSize: String?
    
    var thumbnail: UIImage?
    
    override init() { }
    
    class func buildWithJSON(json: Dictionary<String, AnyObject>) -> ResponseHit {
        
        let respHit = ResponseHit()
        
        respHit.id = json["id"] as? Int
        respHit.pageURL = json["pageURL"] as? String
        respHit.type = json["type"] as? String
        respHit.tags = json["tags"] as? String
        respHit.duration = json["duration"] as? Int
        respHit.pictureId = json["picture_id"] as? String
        respHit.views = json["views"] as? Int
        respHit.downloads = json["downloads"] as? Int
        respHit.favorites = json["favorites"] as? Int
        respHit.likes = json["likes"] as? Int
        respHit.comments = json["comments"] as? Int
        respHit.userId = json["user_id"] as? Int
        respHit.user = json["user"] as? String
        respHit.userImageURL = json["userImageURL"] as? String
        
        if let videos = json["videos"] as? Dictionary<String, AnyObject> {
            
            if let tinyVid = videos["tiny"] as? Dictionary<String, AnyObject> {
                if !(tinyVid["url"] as? String ?? "").isEmpty  {
                    respHit.videoURL = tinyVid["url"] as? String
                    respHit.videoSize = "Tiny"
                }
            }
            
            if let smallVid = videos["small"] as? Dictionary<String, AnyObject> {
                if !(smallVid["url"] as? String ?? "").isEmpty  {
                    respHit.videoURL = smallVid["url"] as? String
                    respHit.videoSize = "Small"
                }
            }
            
            if let mediumVid = videos["medium"] as? Dictionary<String, AnyObject> {
                if !(mediumVid["url"] as? String ?? "").isEmpty  {
                    respHit.videoURL = mediumVid["url"] as? String
                    respHit.videoSize = "Medium"
                }
            }
            
            if let largeVid = videos["large"] as? Dictionary<String, AnyObject> {
                if !(largeVid["url"] as? String ?? "").isEmpty  {
                    respHit.videoURL = largeVid["url"] as? String
                    respHit.videoSize = "Large"
                }
            }
        }
        
        return respHit
    }
}
