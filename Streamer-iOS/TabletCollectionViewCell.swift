//
//  TabletCollectionViewCell.swift
//  Streamer-iOS-CP
//
//  Created by Michael Konapelsky on 7/29/17.
//
//

import UIKit

class TabletCollectionViewCell: UICollectionViewCell {
    
    
    @IBOutlet var thumbNail: UIImageView!
    @IBOutlet var lblViews: UILabel!
    @IBOutlet var lblAuthor: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
}
