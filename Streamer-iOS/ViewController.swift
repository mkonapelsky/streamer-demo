//
//  ViewController.swift
//  Streamer-iOS
//
//  Created by Michael Konapelsky on 7/28/17.
//
//

import UIKit

class ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate, ResponseHitThumbnailDelegate {
    
    private var _searchObject: ResponseObject = ResponseObject()
    @IBOutlet private var resultsTable: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        self.title = "Video Search Demo"
        
        let textField = UITextField(frame: CGRect(x: 0,
                                                  y: 0,
                                                  width: (self.navigationController?.navigationBar.frame.size.width)! / 1.2,
                                                  height: 28.0))
        textField.backgroundColor = UIColor.white
        textField.layer.cornerRadius = 5.0
        textField.placeholder = "Search"
        textField.textAlignment = .center
        textField.keyboardType = .webSearch
        textField.delegate = self
        textField.becomeFirstResponder()
        
        self.navigationItem.titleView = textField
        
        resultsTable.delegate = self
        resultsTable.dataSource = self
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func searchMovies(searchWords: String) {
        
        SVProgressHUD.show(withStatus: "Searching ...")
        
        let web = WebManager()
        web.startGETRequest(uri: "https://pixabay.com/api/videos/?key=6024963-a84e7b583b488d22a4cffe1c0&q=" + searchWords.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!) {
            responseData, responseError in
            
            if responseError != nil
            {
                self.present(Utilities.ShowBasicAlert(title: "Error", message: responseError!, style: .actionSheet), animated: true, completion: nil)
            }
            else
            {
                do {
                    
                    let jsonObject = try JSONSerialization.jsonObject(with: responseData!, options: JSONSerialization.ReadingOptions.allowFragments)
                    
                    self._searchObject = ResponseObject.buildWithJSON(json: jsonObject as! Dictionary<String, AnyObject>)
                    self._searchObject.thumbnailDelegate = self;
                    
                    if self._searchObject.hits.count == 0 {
                        
                        self.present(Utilities.ShowBasicAlert(title: "No Videos",
                                                              message: "Couldn't find videos based on your search results.",
                                                              style: .alert),
                                     animated: true)
                        
                        SVProgressHUD.dismiss()
                        
                    }
                    else {
                        
                        SVProgressHUD.show(withStatus: "Generating Thumbnails ...")
                        self._searchObject.GetVideoThumbnails();
                        
                    }
                }
                catch
                {
                    // Alert
                    self.present(Utilities.ShowBasicAlert(title: "Attention", message: "There was a problem refreshing. {1}", style: .actionSheet), animated: true, completion: nil)
                }
            }
        }
    }
    
    // Table Protocol
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self._searchObject.hits.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var cell = tableView.dequeueReusableCell(withIdentifier: "videoCell") as? VideoTableViewCell
        if cell == nil {
            cell = VideoTableViewCell(style: .default, reuseIdentifier: "videoCell")
        }
        
        let object: ResponseHit = self._searchObject.hits[indexPath.row]
        
        cell?.thumbNail.image = object.thumbnail
        cell?.lblViews.text = "\(object.views!) Views"
        cell?.lblUserName.text = object.user
        
       return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let object: ResponseHit = self._searchObject.hits[indexPath.row]
        
        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let playerView: PlayerViewController = storyBoard.instantiateViewController(withIdentifier: "playerView") as! PlayerViewController
        
        playerView.videoURL = object.videoURL
        
        self.navigationController?.present(playerView, animated: true, completion: nil)
    }
    
    // Text Field Protocol
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        // Perform search, dismiss keyboard
        searchMovies(searchWords: textField.text!)
        textField.resignFirstResponder()
        
        return true
    }
    
    // Thumbnail delegate
    func didFinishBuildingThumbnails(responseObject: ResponseObject) {
        
        SVProgressHUD.dismiss()
        self.resultsTable.reloadData()
    }
}

