//
//  VideoTableViewCell.swift
//  Streamer-iOS-CP
//
//  Created by Michael Konapelsky on 7/28/17.
//
//

import UIKit

class VideoTableViewCell: UITableViewCell {

    @IBOutlet var thumbNail: UIImageView!
    @IBOutlet var lblViews: UILabel!
    @IBOutlet var lblUserName: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
