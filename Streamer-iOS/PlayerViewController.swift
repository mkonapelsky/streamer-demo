//
//  PlayerViewController.swift
//  Streamer-iOS-CP
//
//  Created by Michael Konapelsky on 7/28/17.
//
//

import UIKit
import AVKit
import AVFoundation

class PlayerViewController: UIViewController {

    var videoURL: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        let url: URL = URL(string: videoURL!)!
        let item: AVPlayerItem = AVPlayerItem(url: url)
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(playerDidFinishPlaying),
                                               name: NSNotification.Name.AVPlayerItemDidPlayToEndTime,
                                               object: item)
        
        let player: AVPlayer = AVPlayer(playerItem: item)
        
        let playerController: AVPlayerViewController = AVPlayerViewController()
        playerController.player = player
        
        self.addChildViewController(playerController)
        self.view.addSubview(playerController.view)
        playerController.view.frame = self.view.frame;
                
        player.play()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func playerDidFinishPlaying(note: NSNotification)
    {
        self.dismiss(animated: true)
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self,
                                                  name: NSNotification.Name.AVPlayerItemDidPlayToEndTime,
                                                  object: nil)
    }
}
